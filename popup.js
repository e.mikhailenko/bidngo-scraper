$('#buttonFillForm').on('click', function () {
    fillForm();
});

function fillForm() {
    // Product tab
    let inputPrice = $('#inputPrice');
    let inputName = $('#inputName');
    let inputLot = $('#inputLot');
    let inputDescription = $('#inputDescription');
    let inputQuantity = $('#inputQuantity');
    let inputCondition = $('#inputCondition');
    let inputExtRetail = $('#inputExtRetail');
    let inputShipping = $('#inputShipping');
    let inputImageUrls = $('#inputImageUrls');
    let inputManifestUrl = $('#inputManifestUrl');

    // Auction tab
    // let inputAuctionStatus = $('#inputAuctionStatus');
    let inputAuctionPrice = $('#inputAuctionPrice');
    let inputReservePrice = $('#inputReservePrice');
    let inputAuctionStartTime = $('#inputAuctionStartTime');
    let inputAuctionStopTime = $('#inputAuctionStopTime');
    let inputCouponValidTime = $('#inputCouponValidTime');
    chrome.tabs.executeScript(null, {
        file: "getPagesSource.js"
    }, function () {
        // If you try and inject into an extensions page or the webstore/NTP you'll get an error
        if (chrome.runtime.lastError) {
            alert('There was an error injecting script : \n' + chrome.runtime.lastError.message);
        }
    });
}

chrome.runtime.onMessage.addListener(function (request, sender) {
    if (request.action == "getSource") {
        let source = request.source;
        let holder = $('<div>');
        holder.append(source);
        let title = holder.find('title').text();
        let isCostco = false;
        if (title.indexOf('Costco') !== -1) {
            isCostco = true;
        }
        console.log(isCostco);
        let currentBidAmount = holder.find('#current_bid_amount').text();
        if (currentBidAmount !== undefined) {
            currentBidAmount = currentBidAmount.trim().replace('$', '').replace(',', '');
        }
        let name = holder.find('div.product-name h1').text();
        let lot = name;
        if (lot !== undefined) {
            if (isCostco) {
                lot = lot.split('(')[1].split(')')[0];
            } else {
                lot = lot.split('(')[1].split(')')[0].split(' ')[1];
            }
        }
        let description = holder.find('div.auction-details dl dt:contains("Description")').next().text();
        if (description !== undefined) {
            description = description.trim().replace(/\s+/g, ' ')
        }
        let quantity = holder.find('div.auction-details dl dt:contains("Quantity")').next().text();
        if (quantity !== undefined) {
            if (isCostco) {
                quantity = quantity.trim().split(' ')[0].replace(',', '').replace('.', '');
            } else {
                quantity = quantity.trim().replace(' Units', '');
            }
        }
        let condition = holder.find('div.auction-details dl dt:contains("Condition")').next().text();
        if (condition !== undefined) {
            condition = condition.trim().replace(/\s+/g, ' ')
        }
        let extRetail = holder.find('div.auction-details dl dt:contains("Ext. Retail")').next().text();
        if (extRetail !== undefined) {
            extRetail = extRetail.trim().replace('$', '').replace(',', '');
        }
        let shipping = holder.find('div.auction-details dl dt:contains("Shipping")').next().text();

        let imageUrls = holder.find('.gallery-image');
        let imageUrlsString = '';
        if (imageUrls !== undefined) {
            imageUrlsString = fillImageUrls(imageUrls, imageUrlsString);
        }
        let manifestUrl = holder.find('div.manifest-download button').attr('onclick');
        if (manifestUrl !== undefined) {
            manifestUrl = manifestUrl.split('(\'')[1].split('\');')[0]
        }
        inputPrice.value = currentBidAmount;
        inputName.value = name;
        inputLot.value = lot;
        inputDescription.value = description;
        inputQuantity.value = quantity;
        inputCondition.value = condition;
        inputExtRetail.value = extRetail;
        inputImageUrls.value = imageUrlsString;
        inputManifestUrl.value = manifestUrl;
        if (shipping.indexOf('Pallet') !== -1) {
            inputShipping.value = 'Pallet';
        } else if (shipping.indexOf('Truckload') !== -1) {
            inputShipping.value = 'Truckload';
        } else if (shipping.indexOf('Case') !== -1) {
            inputShipping.value = 'Case';
        }

        let date = new Date();

        inputAuctionStatus.value = 1;
        inputAuctionPrice.value = currentBidAmount;
        inputReservePrice.value = Number(currentBidAmount) + 1000;
        inputAuctionStartTime.value = getISODateTime(date, 1);
        inputAuctionStopTime.value = getISODateTime(date, 20);
        inputCouponValidTime.value = 3;
    }
});

function fillImageUrls(imageUrls, imageUrlsString) {
    imageUrls.each(function (i, elem) {
        if (elem['id'] !== 'image-main') {
            console.log(elem['currentSrc']);
            imageUrlsString += elem['currentSrc'] + '\n';
        }
    })
    return imageUrlsString;
}


$('#buttonSendData').on('click', function () {
    $.ajax({
        url: 'env.txt',
        dataType: 'text',
        success: function (data) {
            let arrayEnv = data.split('\n');
            let apiKey = arrayEnv[0];
            let apiName = arrayEnv[1];
            let localUrl = arrayEnv[2];
            let serverUrl = arrayEnv[3];
            // loginBidngo(apiKey, apiName, localUrl);
            loginBidngo(apiKey, apiName, serverUrl);
        }
    });
});

function loginBidngo(apiKey, apiName, url) {
    let sendData = true;
    $('.form-control').each(function (i, elem) {
        if (elem.value === '' && elem.hasAttribute('required')) {
            console.log('!!!');
            sendData = false;
        }
    });
    if (!sendData) {
        $('#validationMessages').addClass('d-block');
        $('#validationMessages').text('Please fill all the required fields');
    } else {
        $('#validationMessages').removeClass('d-block');
        $.ajax({
            url: url + 'api/login',
            type: 'post',
            dataType: 'json',
            data: {
                username: apiName,
                key: apiKey,
            },
            crossDomain: true,
            async: false,
            cache: false,
            success: function (json) {
                if (json['error']) {
                    $('#validationMessages').text(json['error']);
                }
                if (json['api_token']) {
                    let apiToken = json['api_token'];
                    console.log(apiToken);
                    addProduct(apiToken, url)
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

function addProduct(apiToken, url) {
    console.log('add product begin');
    // console.log(apiToken);
    console.log(inputImageUrls.value);
    $.ajax({
        type: 'post',
        url: url + 'api/product/addFromBstock',
        data: {
            api_token: apiToken,
            product_name: inputName.value,
            price: inputPrice.value,
            lot: inputLot.value,
            description: inputDescription.value,
            quantity: inputQuantity.value,
            condition: inputCondition.value,
            ext_retail: inputExtRetail.value,
            shipping: inputShipping.value,
            image_urls: inputImageUrls.value,
            manifest_url: inputManifestUrl.value,
        },
        dataType: 'json',
        async: false,
        cache: false,
        success: function (json) {
            if (json['success']) {
                console.log('add product');
                console.log(json['product_attribute']);
                console.log(json['image']);
                addAuction(apiToken, json['product_id'], url)
            }
            if (json['error']) {
                $('#validationMessages').text(json['error']);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

function addAuction(apiToken, productId, url) {
    console.log('add auction begin');
    let auctionStatus = 0;
    let checked = $('#inputAuctionStatus').attr('checked');
    if (typeof checked !== typeof undefined && checked !== false) {
        auctionStatus = 1;
    }
    console.log(auctionStatus);
    $.ajax({
        type: 'post',
        url: url + 'api/auction/addFromBstock',
        data: {
            api_token: apiToken,
            product_id: productId,
            auction_status: auctionStatus,
            auction_price: inputAuctionPrice.value,
            reserve_price: inputReservePrice.value,
            start_time: inputAuctionStartTime.value,
            stop_time: inputAuctionStopTime.value,
            coupon_valid_time: inputCouponValidTime.value,
            minimum_quantity: 1,
            maximum_quantity: 1,
            increment_option: 1,
            automatic_option: 0,
        },
        dataType: 'json',
        async: false,
        cache: false,
        success: function (json) {
            if (json['success']) {
                console.log('add auction');
                let auctionId = json['auction_id'];
                let succesText = `Product and Auction successfully added into Bidngo with product_id = ${productId} and auction_id = ${auctionId}`;
                $('#validationMessages').removeClass('invalid-feedback');
                $('#validationMessages').addClass('valid-feedback d-block');
                $('#validationMessages').text(succesText);
            }
            if (json['error']) {
                $('#validationMessages').text(json['error']);
            }
        }
    });
}

function getISODateTime(date, day) {
    // padding function
    var s = function (a, b) {
        return (1e15 + a + "").slice(-b)
    };

    // default date parameter
    if (typeof date === 'undefined') {
        date = new Date();
    }
    ;

    // return ISO datetime
    return date.getFullYear() + '-' +
        s(date.getMonth() + 1, 2) + '-' +
        s(day, 2)
        // + ' ' +
        // s(date.getHours(),2) + ':' +
        // s(date.getMinutes(),2) + ':' +
        // s(date.getSeconds(),2)
        ;
}

